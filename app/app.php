<?php

require_once __DIR__ . '/../app/autoload.php';

$data = json_decode(file_get_contents(__DIR__ . '/../data/data.json'), true);

if (null === $data) {
    printf("Input data invalid\nError while parsing json: %s\n", json_last_error_msg()) ;

    return;
}
// Randomize data
shuffle($data);

$app = new \Boarding\App($cardBuilder, $journeyBuilder);
$app->handle($data);
