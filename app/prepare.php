<?php

$cardFactory = new \Boarding\Card\BoardingCardFactory();
$mapper = \Boarding\ObjectMapper\ObjectMapper::createDefault();

$cardValidator = new \Boarding\Card\Validator\ChainValidator();
$cardValidator->addValidator(new \Boarding\Card\Validator\BusBoardingCardValidator());
$cardValidator->addValidator(new \Boarding\Card\Validator\PlaneBoardingCardValidator());
$cardValidator->addValidator(new \Boarding\Card\Validator\TrainBoardingCardValidator());

$cardBuilder = new \Boarding\Card\CardBuilder($cardFactory, $mapper, $cardValidator);

$outputRegistry = new \Boarding\Output\CardOutputRegistry();
$outputRegistry->addOutput(new \Boarding\Output\BusInstructionOutput());
$outputRegistry->addOutput(new \Boarding\Output\TrainInstructionOutput());
$outputRegistry->addOutput(new \Boarding\Output\PlaneInstructionOutput());

$journeyOutput = new \Boarding\Output\JourneyListOutput($outputRegistry, \Boarding\Output\JourneyListOutput::FINAL_MESSAGE);
$journeyBuilder = new \Boarding\JourneyBuilder($journeyOutput);