<?php

require __DIR__ . '/../vendor/autoload.php';

error_reporting(E_ALL | E_STRICT);

//Turns off error handler
restore_error_handler();
ini_set('max_execution_time', 10000);