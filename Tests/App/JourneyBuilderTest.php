<?php

namespace Tests\App;

use Boarding\Card\BusBoardingCard;
use Boarding\Card\PlaneBoardingCard;
use Boarding\Card\TrainBoardingCard;
use Boarding\JourneyBuilder;
use Boarding\Output\BusInstructionOutput;
use Boarding\Output\CardOutputRegistry;
use Boarding\Output\JourneyListOutput;
use Boarding\Output\PlaneInstructionOutput;
use Boarding\Output\TrainInstructionOutput;
use LogicException;
use SplQueue;

class JourneyBuilderTest extends \PHPUnit\Framework\TestCase
{
    private $expectedMessages = [
        "Take airport bus from Barcelona to Catalonia. From platform 5C. No seat assignment.",
        "From Catalonia airport, take flight SP155 to Madrid. Gate 12, seat 13A.\n Baggage drop at ticket counter 111",
        "Take train 74B from Madrid to Gerona. Sit on seat 23B.",
        JourneyListOutput::FINAL_MESSAGE,
    ];

    /**
     * @var JourneyBuilder
     */
    private $builder;

    protected function setUp()
    {
        parent::setUp();

        $outputRegistry = new CardOutputRegistry();
        $outputRegistry->addOutput(new BusInstructionOutput());
        $outputRegistry->addOutput(new TrainInstructionOutput());
        $outputRegistry->addOutput(new PlaneInstructionOutput());

        $journeyOutput = new JourneyListOutput($outputRegistry, JourneyListOutput::FINAL_MESSAGE);
        $this->builder = new JourneyBuilder($journeyOutput);
    }

    public function buildProvider()
    {
        return [
            [
                [
                    new BusBoardingCard("Barcelona", "Catalonia", null, "5C", null), // First
                    new TrainBoardingCard("Madrid", "Gerona", "74B", null, "23B"), // Third
                    new PlaneBoardingCard("Catalonia", "Madrid", "SP155", "12", "13A", "Baggage drop at ticket counter 111"), //Second
                ],
                $this->expectedMessages,
            ],
            [
                [
                    new TrainBoardingCard("Madrid", "Gerona", "74B", null, "23B"), // Third
                    new PlaneBoardingCard("Catalonia", "Madrid", "SP155", "12", "13A", "Baggage drop at ticket counter 111"), //Second
                    new BusBoardingCard("Barcelona", "Catalonia", null, "5C", null), // First
                ],
                $this->expectedMessages,
            ],
            [
                [
                    new PlaneBoardingCard("Catalonia", "Madrid", "SP155", "12", "13A", "Baggage drop at ticket counter 111"), //Second
                    new TrainBoardingCard("Madrid", "Gerona", "74B", null, "23B"), // Third
                    new BusBoardingCard("Barcelona", "Catalonia", null, "5C", null), // First
                ],
                $this->expectedMessages,
            ]
        ];
    }

    public function buildProviderFail()
    {
        return [
            [
                [
                    new BusBoardingCard("Barcelona", "Catalonia", null, "5C", null), // First
                    new TrainBoardingCard("Berlin", "Gerona", "74B", null, "23B"), // Third
                    new PlaneBoardingCard("Catalonia", "Madrid", "SP155", "12", "13A", "Baggage drop at ticket counter 111"), //Second
                ],
                $this->expectedMessages,
            ],
        ];
    }

    /**
     * @dataProvider buildProvider
     *
     * @param array $cards
     * @param array $expectedMessages
     */
    public function testBuild(array $cards, array $expectedMessages)
    {
        $queue = new SplQueue();
        foreach ($cards as $card) {
            $queue->enqueue($card);
        }


        $messages = $this->builder->build($queue);

        $this->assertTrue(is_array($messages));
        $this->assertEquals(count($cards) + 1, count($messages));
        $this->assertEquals($expectedMessages, $messages);
    }

    /**
     * @dataProvider buildProviderFail
     * @expectedException LogicException
     *
     * @param array $cards
     * @param array $expectedMessages
     */
    public function testBuildFail(array $cards, array $expectedMessages)
    {
        $queue = new SplQueue();
        foreach ($cards as $card) {
            $queue->enqueue($card);
        }

        $this->builder->build($queue);
    }
}