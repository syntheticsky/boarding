<?php

namespace Boarding\Exception;

use ArrayIterator;
use Exception;
use Iterator;
use IteratorAggregate;
use Traversable;

/**
 * Class CardNotValidListException
 *
 * Indicate that card is not valid
 *
 * @author constantine.shtompel@gmail.com
 */
class CardNotValidListException extends Exception implements IteratorAggregate
{
    /**
     * @var CardNotValidException[]
     */
    private $exceptions;

    public function addException(CardNotValidException $exception): void
    {
        $this->exceptions[] = $exception;
    }

    /**
     * Retrieve an external iterator
     *
     * @link  http://php.net/manual/en/iteratoraggregate.getiterator.php
     * @return Traversable An instance of an object implementing <b>Iterator</b> or
     * <b>Traversable</b>
     * @since 5.0.0
     */
    public function getIterator(): Iterator
    {
        return new ArrayIterator($this->exceptions);
    }

    /**
     * Checks empty
     *
     * @return bool
     */
    public function isEmpty(): bool
    {
        return empty($this->exceptions);
    }
}
