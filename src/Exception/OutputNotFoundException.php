<?php

namespace Boarding\Exception;

use Exception;

/**
 * Class OutputNotFoundException
 *
 * @author constantine.shtompel@gmail.com
 */
class OutputNotFoundException extends Exception
{
}
