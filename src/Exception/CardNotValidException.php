<?php

namespace Boarding\Exception;

use Exception;

/**
 * Class CardNotValidException
 *
 * Indicate that card is not valid
 *
 * @author constantine.shtompel@gmail.com
 */
class CardNotValidException extends Exception
{
}
