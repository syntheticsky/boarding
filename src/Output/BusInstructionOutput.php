<?php

namespace Boarding\Output;

use Boarding\Card\AbstractBoardingCard;
use Boarding\Card\BoardingCardInterface;
use Boarding\Card\BusBoardingCard;

/**
 * Class BusInstructionOutput
 *
 * @author constantine.shtompel@gmail.com
 */
class BusInstructionOutput implements BoardingCardOutputInterface
{
    /**
     * {@inheritdoc}
     * @see BoardingCardOutputInterface::supports()
     */
    public function supports(BoardingCardInterface $card): bool
    {
        return $card instanceof BusBoardingCard;
    }

    /**
     * {@inheritdoc}
     * @see BoardingCardOutputInterface::output()
     */
    public function output(
        BoardingCardInterface $card,
        BoardingCardInterface $prevCard = null,
        BoardingCardInterface $nextCard = null
    ): string
    {
        /** @var BusBoardingCard $card */
        $pattern = 'Take %s%s%sfrom %s to %s.';
        $args = [
            !empty($nextCard) && AbstractBoardingCard::TYPE_PLANE === $nextCard->getTransport()
                ? 'airport ' : '',
            $card->getTransport() . ' ',
            $card->getNumber() ? $card->getNumber() . ' ' : '',
            $card->getFrom(),
            $card->getTo(),
        ];

        if (!empty($card->getPlatform())) {
            $pattern .= ' From platform %s.';
            $args[] = $card->getPlatform();
        }

        if (!empty($card->getSeat())) {
            $pattern .= ' Sit on seat %s.';
            $args[] = $card->getSeat();
        } else {
            $pattern .= ' No seat assignment.';
        }

        return sprintf($pattern, ...$args);
    }
}