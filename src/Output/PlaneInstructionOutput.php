<?php

namespace Boarding\Output;

use Boarding\Card\AbstractBoardingCard;
use Boarding\Card\BoardingCardInterface;
use Boarding\Card\PlaneBoardingCard;

/**
 * Class PlaneInstructionOutput
 *
 * @author constantine.shtompel@gmail.com
 */
class PlaneInstructionOutput implements BoardingCardOutputInterface
{
    /**
     * {@inheritdoc}
     * @see BoardingCardOutputInterface::supports()
     */
    public function supports(BoardingCardInterface $card): bool
    {
        return $card instanceof PlaneBoardingCard;
    }

    /**
     * {@inheritdoc}
     * @see BoardingCardOutputInterface::output()
     */
    public function output(
        BoardingCardInterface $card,
        BoardingCardInterface $prevCard = null,
        BoardingCardInterface $nextCard = null
    ): string
    {
        /** @var PlaneBoardingCard $card */
        $pattern = 'From %s airport, take flight %s to %s.';
        $args = [
            $card->getFrom(),
            $card->getNumber(),
            $card->getTo(),
        ];

        $pattern .= ' Gate %s, seat %s.';
        $args[] = $card->getPlatform();
        $args[] = $card->getSeat();
        $pattern .= "\n %s";
        $args[] = $card->getBaggageInstruction();

        return sprintf($pattern, ...$args);
    }
}