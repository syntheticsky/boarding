<?php

namespace Boarding\Output;

/**
 * Class OutputInterface
 *
 * @author constantine.shtompel@gmail.com
 */
interface OutputInterface
{
    /**
     * Output data in some format
     *
     * @return mixed
     */
    public function output();
}