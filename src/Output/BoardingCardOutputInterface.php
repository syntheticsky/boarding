<?php

namespace Boarding\Output;

use Boarding\Card\BoardingCardInterface;

/**
 * Interface BoardingCardOutputInterface
 *
 * Use it to define boarding cards re-presenters
 *
 * @author constantine.shtompel@gmail.com
 */
interface BoardingCardOutputInterface
{
    /**
     * Checks whether card is supported or not
     *
     * @param BoardingCardInterface $card
     *
     * @return bool
     */
    public function supports(BoardingCardInterface $card): bool;

    /**
     * Represents boarding card in some format
     *
     * @param BoardingCardInterface $card     Current card
     *
     * @param BoardingCardInterface $prevCard optional Previous card
     * @param BoardingCardInterface $nextCard optional Next card
     *
     * @return mixed
     */
    public function output(
        BoardingCardInterface $card,
        BoardingCardInterface $prevCard = null,
        BoardingCardInterface $nextCard = null
    );
}
