<?php

namespace Boarding\Output;

use Boarding\Card\BoardingCardInterface;
use Boarding\Exception\OutputNotFoundException;


/**
 * Class StringInstructionOutput
 *
 * @author constantine.shtompel@gmail.com
 */
class CardOutputRegistry
{
    /**
     * @var array|BoardingCardOutputInterface[]
     */
    private $registry;

    /**
     * @param BoardingCardOutputInterface $output
     */
    public function addOutput(BoardingCardOutputInterface $output): void
    {
        $this->registry[] = $output;
    }

    /**
     * Gets output for the card
     *
     * @param BoardingCardInterface $card
     *
     * @return BoardingCardOutputInterface
     * @throws OutputNotFoundException
     */
    public function getOutput(BoardingCardInterface $card): BoardingCardOutputInterface
    {
        foreach ($this->registry as $item) {
            if ($item->supports($card)) {
                return $item;
            }
        }

        throw new OutputNotFoundException('Card output not found. ' . $card->getTransport());
    }
}
