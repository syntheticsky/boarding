<?php

namespace Boarding\Output;


use Boarding\Card\BoardingCardInterface;
use Iterator;
use IteratorAggregate;
use SplQueue;
use Traversable;

/**
 * Class JourneyListOutput
 *
 * @author constantine.shtompel@gmail.com
 */
class JourneyListOutput implements OutputInterface
{
    const FINAL_MESSAGE = 'You have arrived at your final destination.';

    /**
     * @var SplQueue|BoardingCardInterface[]
     */
    private $cards;

    /**
     * @var CardOutputRegistry
     */
    private $outputs;

    /**
     * @var string
     */
    private $finalMessage;

    /**
     * JourneyListOutput constructor.
     *
     * @param CardOutputRegistry $outputs
     * @param string             $finalMessage
     */
    public function __construct(CardOutputRegistry $outputs, string $finalMessage)
    {
        $this->outputs = $outputs;
        $this->finalMessage = $finalMessage;
    }

    /**
     * @param SplQueue|BoardingCardInterface[] $cards
     *
     * @return JourneyListOutput
     */
    public function setCards(SplQueue $cards)
    {
        $this->cards = $cards;

        return $this;
    }

    /**
     * Represents journey by cards stack
     * {@inheritdoc}
     * @see OutputInterface::output()
     */
    public function output(): array
    {
        $result = [];
        $prev = null;

        foreach ($this->cards as $key => $card) {
            $next = null;
            /* @var BoardingCardInterface $card */
            if ($this->cards->offsetExists($key + 1)) {
                $next = $this->cards->offsetGet($key + 1);
            }
            $output = $this->outputs->getOutput($card);
            $result[] = $output->output($card, $prev, $next);
            /* @var BoardingCardInterface $prev */
            $prev = $card;
        }

        $result[] = $this->finalMessage;

        return $result;
    }
}
