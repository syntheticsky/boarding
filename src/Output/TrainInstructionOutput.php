<?php

namespace Boarding\Output;

use Boarding\Card\AbstractBoardingCard;
use Boarding\Card\BoardingCardInterface;
use Boarding\Card\TrainBoardingCard;

/**
 * Class TrainInstructionOutput
 *
 * @author constantine.shtompel@gmail.com
 */
class TrainInstructionOutput implements BoardingCardOutputInterface
{
    /**
     * {@inheritdoc}
     * @see BoardingCardOutputInterface::supports()
     */
    public function supports(BoardingCardInterface $card): bool
    {
        return $card instanceof TrainBoardingCard;
    }

    /**
     * {@inheritdoc}
     * @see BoardingCardOutputInterface::output()
     */
    public function output(
        BoardingCardInterface $card,
        BoardingCardInterface $prevCard = null,
        BoardingCardInterface $nextCard = null
    ): string
    {
        /** @var TrainBoardingCard $card */
        $pattern = 'Take %s %s from %s to %s.';
        $args = [
            $card->getTransport(),
            $card->getNumber(),
            $card->getFrom(),
            $card->getTo(),
        ];

        if (!empty($card->getPlatform())) {
            $pattern .= ' From line %s.';
            $args[] = $card->getPlatform();
        }

        $pattern .= ' Sit on seat %s.';
        $args[] = $card->getSeat();

        return sprintf($pattern, ...$args);
    }
}