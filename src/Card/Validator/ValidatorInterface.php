<?php

namespace Boarding\Card\Validator;

use Boarding\Card\BoardingCardInterface;
use Boarding\Exception\CardNotValidListException;

/**
 * Interface ValidatorInterface
 *
 * @author constantine.shtompel@gmail.com
 */
interface ValidatorInterface
{
    /**
     * Checks that validator supports this card
     *
     * @param BoardingCardInterface $card
     *
     * @return bool
     */
    public function supports(BoardingCardInterface $card): bool;

    /**
     * Validate card. Throws exception on error
     *
     * @param BoardingCardInterface $card
     *
     * @throws CardNotValidListException
     */
    public function validate(BoardingCardInterface $card): void;
}
