<?php

namespace Boarding\Card\Validator;


use Boarding\Card\BoardingCardInterface;
use Boarding\Exception\CardNotValidException;
use Boarding\Exception\CardNotValidListException;
use InvalidArgumentException;

class ChainValidator implements ValidatorInterface
{
    /**
     * @var array|ValidatorInterface[]
     */
    private $validators;

    /**
     * {@inheritdoc}
     * @see ValidatorInterface::supports()
     */
    public function supports(BoardingCardInterface $card): bool
    {
        return true;
    }

    /**
     * {@inheritdoc}
     * @see ValidatorInterface::validate()
     */
    public function validate(BoardingCardInterface $card): void
    {
        try {
            foreach ($this->validators as $validator) {
                if ($validator->supports($card)) {
                    $validator->validate($card);
                    return;
                }
            }
        } catch (CardNotValidListException $e) {
            throw new CardNotValidException(
                sprintf(
                    "%sMessages: %s",
                    PHP_EOL,
                    implode(PHP_EOL, array_map(function (CardNotValidException $e) {
                        return $e->getMessage();
                    }, iterator_to_array($e->getIterator())))
                ));
        }

        throw new InvalidArgumentException(sprintf("Card '%s' not supported by validator", $card->getTransport()));
    }

    /**
     * @param ValidatorInterface $validator
     */
    public function addValidator(ValidatorInterface $validator): void
    {
        $this->validators[] = $validator;
    }
}
