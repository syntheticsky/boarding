<?php

namespace Boarding\Card\Validator;

use Boarding\Card\AbstractBoardingCard;
use Boarding\Card\BoardingCardInterface;
use Boarding\Exception\CardNotValidException;
use Boarding\Exception\CardNotValidListException;

/**
 * Class TrainBoardingCardValidator
 *
 * Bus boarding card
 *
 * @author constantine.shtompel@gmail.com
 */
class TrainBoardingCardValidator implements ValidatorInterface
{
    /**
     * {@inheritdoc}
     * @see ValidatorInterface::validate()
     */
    public function validate(BoardingCardInterface $card): void
    {
        $listErrors = new CardNotValidListException();

        if (empty($card->getFrom())) {
            $listErrors->addException(new CardNotValidException(sprintf(
                'Field "from" is required for %s card',
                $card->getTransport()
            )));
        }

        if (empty($card->getTo())) {
            $listErrors->addException(new CardNotValidException(sprintf(
                'Field "to" is required for %s card',
                $card->getTransport()
            )));
        }

        if (empty($card->getNumber())) {
            $listErrors->addException(new CardNotValidException(sprintf(
                'Field "number" is required for %s card',
                $card->getTransport()
            )));
        }

        if (empty($card->getSeat())) {
            $listErrors->addException(new CardNotValidException(sprintf(
                'Field "seat" is required for %s card',
                $card->getTransport()
            )));
        }

        if (!$listErrors->isEmpty()) {
            throw $listErrors;
        }
    }

    /**
     * {@inheritdoc}
     * @see ValidatorInterface::supports()
     */
    public function supports(BoardingCardInterface $card): bool
    {
        return $card->getTransport() === AbstractBoardingCard::TYPE_TRAIN;
    }
}
