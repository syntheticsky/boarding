<?php

namespace Boarding\Card;

use InvalidArgumentException;

/**
 * Class BoardingCardFactory
 *
 * @author constantine.shtompel@gmail.com
 */
class BoardingCardFactory
{
    private $types = [
        AbstractBoardingCard::TYPE_BUS   => BusBoardingCard::class,
        AbstractBoardingCard::TYPE_PLANE => PlaneBoardingCard::class,
        AbstractBoardingCard::TYPE_TRAIN => TrainBoardingCard::class,
    ];

    /**
     * @param CardBuilderRequest $request
     *
     * @return BoardingCardInterface
     */
    public function getBoardingCard(CardBuilderRequest $request): BoardingCardInterface
    {
        $type = $request->getType();
        if (!array_key_exists($type, $this->types)) {
            throw new InvalidArgumentException(sprintf('Type "%s" not supported yet', $type));
        }

        $class = $this->types[$type];

        return new $class(
            $request->getFrom(),
            $request->getTo(),
            $request->getNumber(),
            $request->getPlatform(),
            $request->getSeat(),
            $request->getBaggage()
        );
    }
}
