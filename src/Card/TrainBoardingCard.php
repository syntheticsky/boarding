<?php

namespace Boarding\Card;

/**
 * Class TrainBoardingCard
 *
 * Bus boarding card
 *
 * @author constantine.shtompel@gmail.com
 */
class TrainBoardingCard extends AbstractBoardingCard
{
    /**
     * {@inheritdoc}
     * @see BoardingCardInterface::getTransport()
     */
    public function getTransport(): string
    {
        return static::TYPE_TRAIN;
    }

    /**
     * {@inheritdoc}
     * @see BoardingCardInterface::getNumber()
     */
    public function getNumber(): ?string
    {
        return $this->number;
    }

    /**
     * {@inheritdoc}
     * @see BoardingCardInterface::getPlatform()
     */
    public function getPlatform(): ?string
    {
        return $this->platform;
    }
}
