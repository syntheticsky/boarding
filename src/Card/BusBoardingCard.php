<?php

namespace Boarding\Card;


/**
 * Class BusBoardingCard
 *
 * Bus boarding card
 *
 * @author constantine.shtompel@gmail.com
 */
class BusBoardingCard extends AbstractBoardingCard
{
    /**
     * {@inheritdoc}
     * @see BoardingCardInterface::getTransport()
     */
    public function getTransport(): string
    {
        return static::TYPE_BUS;
    }

    /**
     * {@inheritdoc}
     * @see BoardingCardInterface::getNumber()
     */
    public function getNumber(): ?string
    {
        return $this->number;
    }

    /**
     * {@inheritdoc}
     * @see BoardingCardInterface::getPlatform()
     */
    public function getPlatform(): ?string
    {
        return $this->platform;
    }
}
