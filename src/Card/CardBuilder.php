<?php

namespace Boarding\Card;

use Boarding\Card\Validator\ValidatorInterface;
use Boarding\Exception\CardNotValidException;
use Boarding\Exception\CardNotValidListException;
use Boarding\ObjectMapper\ObjectMapper;
use InvalidArgumentException;

/**
 * Class CardBuilder
 *
 * @author constantine.shtompel@gmail.com
 */
class CardBuilder
{
    /**
     * @var BoardingCardFactory
     */
    private $cardFactory;

    /**
     * @var ObjectMapper
     */
    private $mapper;

    /**
     * @var CardBuilderRequest
     */
    private $request;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * CardBuilder constructor.
     *
     * @param BoardingCardFactory $cardFactory
     * @param ObjectMapper        $mapper
     * @param ValidatorInterface  $validator
     */
    public function __construct(BoardingCardFactory $cardFactory, ObjectMapper $mapper, ValidatorInterface $validator)
    {
        $this->cardFactory = $cardFactory;
        $this->mapper = $mapper;
        $this->validator = $validator;
    }

    /**
     * @param array $cardData
     *
     * @return BoardingCardInterface
     * @throws CardNotValidException If card not valid
     */
    public function build($cardData): BoardingCardInterface
    {
        $this->request = new CardBuilderRequest();
        $this->mapData($cardData);
        $this->validateRequest();

        $card = $this->cardFactory->getBoardingCard($this->request);

        $this->validator->validate($card);

        return $card;
    }

    /**
     * Check that request is valid
     */
    private function validateRequest(): void
    {
        if (empty($this->request->getType()) || empty($this->request->getFrom()) || empty($this->request->getTo())) {
            throw new InvalidArgumentException(sprintf(
                'Input request should contain at least "type", "from", "to" keys. Given request: %s',
                var_export($this->request, true)
            ));
        }
    }

    /**
     * Map data to request
     *
     * @param $cardData
     */
    private function mapData($cardData): void
    {
        $this->mapper->map($this->request, $cardData);
    }
}