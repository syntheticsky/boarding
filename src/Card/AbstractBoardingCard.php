<?php

namespace Boarding\Card;

/**
 * Class AbstractBoardingCard
 *
 * @author constantine.shtompel@gmail.com
 */
abstract class AbstractBoardingCard implements BoardingCardInterface
{
    const TYPE_BUS = 'bus';
    const TYPE_PLANE = 'plane';
    const TYPE_TRAIN = 'train';

    /**
     * @var string
     */
    protected $from;

    /**
     * @var string
     */
    protected $to;

    /**
     * @var string
     */
    protected $seat;

    /**
     * @var string
     */
    protected $number;

    /**
     * @var string
     */
    protected $platform;

    /**
     * AbstractBoardingCard constructor.
     *
     * @param string $from     Start point
     * @param string $to       Destination point
     * @param string $number   Line/flight number
     * @param string $platform Platform/terminal number
     * @param string $seat     Seat number
     */
    public function __construct($from, $to, $number, $platform, $seat)
    {
        $this->from = $from;
        $this->to = $to;
        $this->seat = $seat;
        $this->number = $number;
        $this->platform = $platform;
    }

    /**
     * {@inheritdoc}
     * @see BoardingCardInterface::getForm()
     */
    public function getFrom(): string
    {
        return $this->from;
    }

    /**
     * {@inheritdoc}
     * @see BoardingCardInterface::getTo()
     */
    public function getTo(): string
    {
        return $this->to;
    }

    /**
     * {@inheritdoc}
     * @see BoardingCardInterface::getSeat()
     */
    public function getSeat(): ?string
    {
        return $this->seat;
    }

    /**
     * {@inheritdoc}
     * @see BoardingCardInterface::getSeat()
     */
    abstract public function getTransport(): string;
}
