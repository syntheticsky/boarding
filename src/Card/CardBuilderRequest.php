<?php

namespace Boarding\Card;


class CardBuilderRequest
{
    /**
     * @var string
     */
    private $type;

    /**
     * @var string
     */
    private $from;

    /**
     * @var string
     */
    private $to;

    /**
     * @var string
     */
    private $seat;

    /**
     * @var string
     */
    private $number;

    /**
     * @var string
     */
    private $platform;

    /**
     * @var string
     */
    private $baggage;

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getFrom(): string
    {
        return $this->from;
    }

    /**
     * @return string
     */
    public function getTo(): string
    {
        return $this->to;
    }

    /**
     * @return string
     */
    public function getSeat(): ?string
    {
        return $this->seat;
    }

    /**
     * @return string
     */
    public function getNumber(): ?string
    {
        return $this->number;
    }

    /**
     * @return string
     */
    public function getPlatform(): ?string
    {
        return $this->platform;
    }

    /**
     * @return string
     */
    public function getBaggage(): ?string
    {
        return $this->baggage;
    }
}