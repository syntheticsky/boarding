<?php

namespace Boarding\Card;

/**
 * Interface BoardingCardBaggagableInterface
 *
 * Used to show how communicate with boarding cards that support baggage information
 *
 * @author constantine.shtompel@gmail.com
 */
interface BoardingCardBaggagableInterface
{
    /**
     * Baggage delivery information
     *
     * @return string
     */
    public function getBaggageInstruction(): string;
}
