<?php

namespace Boarding\Card;

/**
 * Class PlaneBoardingCard
 *
 * Plane boarding card
 *
 * @author constantine.shtompel@gmail.com
 */
class PlaneBoardingCard extends AbstractBoardingCard implements BoardingCardBaggagableInterface
{
    /**
     * @var string
     */
    protected $baggage;

    /**
     * AbstractBoardingCard constructor.
     *
     * @param string $from     Start point
     * @param string $to       Destination point
     * @param string $number   Line/flight number
     * @param string $platform Platform/terminal number
     * @param string $seat     Seat number
     * @param string $baggage  Baggage information
     */
    public function __construct($from, $to, $number, $platform, $seat, string $baggage)
    {
        parent::__construct($from, $to, $number, $platform, $seat);
        $this->baggage = $baggage;
    }

    /**
     * {@inheritdoc}
     * @see BoardingCardInterface::getTransport()
     */
    public function getTransport(): string
    {
        return static::TYPE_PLANE;
    }

    /**
     * {@inheritdoc}
     * @see BoardingCardInterface::getNumber()
     */
    public function getNumber(): string
    {
        return $this->number;
    }

    /**
     * {@inheritdoc}
     * @see BoardingCardInterface::getPlatform()
     */
    public function getPlatform(): string
    {
        return $this->platform;
    }

    /**
     * Baggage delivery information
     */
    public function getBaggageInstruction(): string
    {
        return $this->baggage;
    }
}
