<?php

namespace Boarding\Card;

use Boarding\Exception\CardNotValidListException;

/**
 * Interface BoardingCardInterface
 *
 * Used to show how communicate with boarding cards
 *
 * @author constantine.shtompel@gmail.com
 */
interface BoardingCardInterface
{
    /**
     * Gets start point
     *
     * @return string
     */
    public function getFrom(): string;

    /**
     * Gets destination point
     *
     * @return string
     */
    public function getTo(): string;

    /**
     * Gets transport type
     *
     * @return string
     */
    public function getTransport(): string;

    /**
     * Gets flight/line number
     *
     * @return string
     */
    public function getNumber(): ?string;

    /**
     * Gets platform/terminal number
     *
     * @return string
     */
    public function getPlatform(): ?string;

    /**
     * Gets seat number
     *
     * @return string|null
     */
    public function getSeat(): ?string;
}
