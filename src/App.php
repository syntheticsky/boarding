<?php

namespace Boarding;

use ArrayIterator;
use Boarding\Card\CardBuilder;
use Boarding\Card\BoardingCardInterface;
use Exception;
use InvalidArgumentException;
use SplQueue;

/**
 * Class App
 *
 * @author constantine.shtompel@gmail.com
 */
class App
{
    /**
     * @var BoardingCardInterface[]
     */
    private $cards = [];

    /**
     * @var CardBuilder
     */
    private $cardBuilder;

    /**
     * @var JourneyBuilder
     */
    private $journeyBuilder;

    /**
     * App constructor.
     *
     * @param CardBuilder    $cardBuilder
     * @param JourneyBuilder $journeyBuilder
     *
     * @internal param array $data Initial data
     */
    public function __construct(CardBuilder $cardBuilder, JourneyBuilder $journeyBuilder)
    {
        $this->cardBuilder = $cardBuilder;
        $this->journeyBuilder = $journeyBuilder;
    }

    /**
     * Run application
     *
     * @param array $data
     *
     * @throws Exception
     */
    public function handle(array $data)
    {
        if (empty($data)) {
            throw new InvalidArgumentException('Input data is empty. Please provide correct list of boarding cards.');
        }

        try {
            $this->buildCards($data);
            $result = $this->journeyBuilder->build($this->cards);

            foreach ($result as $item) {
                echo $item . PHP_EOL;
            }
        } catch (Exception $e) {
            echo $e;
            return;
        }
    }

    private function buildCards(array $data)
    {
        $this->cards = new SplQueue();
        foreach ($data as $item) {
            $this->cards->enqueue($this->cardBuilder->build($item));
        }
    }
}