<?php

namespace Boarding;

use Boarding\Card\BoardingCardInterface;
use Boarding\Output\JourneyListOutput;
use LogicException;
use SplQueue;

/**
 * Class JourneyBuilder
 *
 * @author constantine.shtompel@gmail.com
 */
class JourneyBuilder
{
    /**
     * Array to store data keyed by "from" point
     *
     * @var array
     */
    private $from = [];

    /**
     * Array to store data keyed by "to" point
     *
     * @var array
     */
    private $to = [];

    /**
     * @var JourneyListOutput
     */
    private $journeyOutput;

    /**
     * JourneyBuilder constructor.
     *
     * @param JourneyListOutput $journeyOutput
     */
    public function __construct(JourneyListOutput $journeyOutput)
    {
        $this->journeyOutput = $journeyOutput;
    }

    /**
     * @param SplQueue|BoardingCardInterface[] $cards
     *
     * @return array
     */
    public function build(SplQueue $cards)
    {
        $cards = $this->cardsOrder($cards);
        $this->journeyOutput->setCards($cards);

        return $this->journeyOutput->output();
    }

    /**
     * @param SplQueue|BoardingCardInterface[] $cards
     *
     * @return SplQueue
     */
    private function cardsOrder(SplQueue $cards)
    {
        $count = $cards->count();

        if (!$cards->isEmpty()) {
            $first = $cards->bottom();

            foreach ($cards as $card) {
                $cards->dequeue();
                $this->from[$card->getFrom()] = $card;
                $this->to[$card->getTo()] = $card;
            }

            $cards->enqueue($first);
            // Find all trip points before this first element
            $this->findTo($cards);
            $this->findFrom($cards);

            if ($cards->count() !== $count) {
                throw new LogicException("Input data is not valid. Could not build travel path.");
            }
        }

        return $cards;
    }

    /**
     * @param SplQueue $cards
     */
    private function findFrom(SplQueue $cards): void
    {
        /* @var BoardingCardInterface $last */
        $last = $cards->top();
        if (isset($this->from[$last->getTo()])) {
            $fromLast = $this->from[$last->getTo()];
            /* @var BoardingCardInterface $fromLast */
            $cards->push($fromLast);
            unset($this->from[$last->getTo()], $this->to[$fromLast->getFrom()]);
            $this->findFrom($cards);
        }

        return;
    }

    /**
     * @param SplQueue $cards
     */
    private function findTo(SplQueue $cards)
    {
        /* @var BoardingCardInterface $first */
        $first = $cards->bottom();
        if (isset($this->to[$first->getFrom()])) {
            $toFirst = $this->to[$first->getFrom()];
            /* @var BoardingCardInterface $toItem */
            $cards->unshift($toFirst);
            unset($this->to[$first->getFrom()], $this->from[$toFirst->getTo()]);
            // Do the same while have ancestors in graph
            $this->findTo($cards);
        }

        return;
    }
}
