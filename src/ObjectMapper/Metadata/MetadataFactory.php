<?php

namespace Boarding\ObjectMapper\Metadata;

use Boarding\ObjectMapper\Metadata\Loader\LoaderInterface;


/**
 * Base metadata factory
 *
 * @author constantine.shtompel@gmail.com
 */
class MetadataFactory implements MetadataFactoryInterface
{
    /**
     * @var LoaderInterface
     */
    private $loader;

    /**
     * Construct
     *
     * @param LoaderInterface $loader
     */
    public function __construct(LoaderInterface $loader)
    {
        $this->loader = $loader;
    }

    /**
     * {@inheritDoc}
     */
    public function load($object, $group)
    {
        return $this->loader->load($object, $group);
    }
}
