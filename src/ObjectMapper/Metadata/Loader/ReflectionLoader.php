<?php

namespace Boarding\ObjectMapper\Metadata\Loader;

use Boarding\ObjectMapper\Annotation\Property;
use Boarding\ObjectMapper\Metadata\CollectionMetadata;
use Boarding\ObjectMapper\Metadata\ObjectMetadata;
use Boarding\ObjectMapper\Metadata\PropertyMetadata;
use Boarding\ObjectMapper\Reflection;
use ReflectionProperty;


/**
 * Reflection loader
 *
 * @author constantine.shtompel@gmail.com
 */
class ReflectionLoader implements LoaderInterface
{
    /**
     * {@inheritDoc}
     */
    public function load($object, $group)
    {
        $reflection = Reflection::loadClassReflection($object);

        $propertiesMappingAnnotation = $this->getAllPropertiesMapping($reflection, $group);

        if (!$propertiesMappingAnnotation) {
            return null;
        }

        $strategy = ObjectMetadata::STRATEGY_REFLECTION;

        $properties = array();
        foreach ($propertiesMappingAnnotation as $propertyName => $propertyAnnotation) {
            $fieldName = $propertyAnnotation->fieldName ?: $propertyName;

            if ($propertyAnnotation->collection) {
                $collection = new CollectionMetadata(
                    $propertyAnnotation->collection->class,
                    $propertyAnnotation->collection->saveKeys
                );
            } else {
                $collection = null;
            }

            $property = new PropertyMetadata($propertyName, $fieldName, $propertyAnnotation->class, $collection);

            $properties[] = $property;
        }

        $objectMetadata = new ObjectMetadata($strategy, $properties);

        return $objectMetadata;
    }


    /**
     * Get all properties for mapping
     *
     * @param \ReflectionClass $refClass
     * @param string           $group
     *
     * @return Property[]
     */
    public function getAllPropertiesMapping(\ReflectionClass $refClass, $group)
    {
        $reflectionProperties = $refClass->getProperties(
            ReflectionProperty::IS_PRIVATE | ReflectionProperty::IS_PROTECTED | ReflectionProperty::IS_PUBLIC
        );

        $properties = [];

        foreach ($reflectionProperties as $reflectionProperty) {
            $property = new Property([]);
            $property->fieldName = $reflectionProperty->getName();
            $property->groups = [$group];

            $properties[$reflectionProperty->getName()] = $property;
        }

        return $properties;
    }
}