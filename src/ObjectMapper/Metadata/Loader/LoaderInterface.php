<?php

namespace Boarding\ObjectMapper\Metadata\Loader;

use Boarding\ObjectMapper\Metadata\ObjectMetadata;

/**
 * All data mapper metadata loaders should be implemented of this interface
 *
 * @author constantine.shtompel@gmail.com
 */
interface LoaderInterface
{
    /**
     * Load metadata for object and group
     *
     * @param object $object
     * @param string $group
     *
     * @return ObjectMetadata
     */
    public function load($object, $group);
}