<?php

namespace Boarding\ObjectMapper\Metadata;


/**
 * All metadata factories should be implemented of this interface
 *
 * @author constantine.shtompel@gmail.com
 */
interface MetadataFactoryInterface
{
    /**
     * Load metadata for object
     *
     * @param object $object
     * @param string $group
     *
     * @return ObjectMetadata|null
     */
    public function load($object, $group);
}