<?php

namespace Boarding\ObjectMapper\Strategy;

use Boarding\ObjectMapper\Exception\StrategyNotFoundException;


/**
 * All strategy managers should be implemented of this interface
 *
 * @author constantine.shtompel@gmail.com
 */
interface StrategyRegistryInterface
{
    /**
     * Get strategy
     *
     * @param string $key
     *
     * @return StrategyInterface
     *
     * @throws StrategyNotFoundException
     */
    public function get($key);
}