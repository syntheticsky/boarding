<?php

namespace Boarding\ObjectMapper\Strategy;

use Boarding\Exception\UnexpectedTypeException;
use Boarding\ObjectMapper\Metadata\PropertyMetadata;
use Boarding\ObjectMapper\Reflection;


/**
 * Reflection strategy mapping
 *
 * @author constantine.shtompel@gmail.com
 */
class ReflectionStrategy implements StrategyInterface
{
    /**
     * {@inheritDoc}
     */
    public function map(PropertyMetadata $property, $object, $value)
    {
        if (!is_object($object)) {
            throw UnexpectedTypeException::create($object, 'object');
        }

        if (!$property->reflection) {
            $objectReflection = Reflection::loadObjectReflection($object);
            $propertyName = $property->getPropertyName();
            $propertyReflection = $objectReflection->getProperty($propertyName);

            if (!$propertyReflection->isPublic()) {
                $propertyReflection->setAccessible(true);
            }

            $property->reflection = $propertyReflection;
        }

        $property->reflection->setValue($object, $value);
    }
}