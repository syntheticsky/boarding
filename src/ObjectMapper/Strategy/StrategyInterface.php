<?php

namespace Boarding\ObjectMapper\Strategy;

use Boarding\ObjectMapper\Metadata\PropertyMetadata;


/**
 * All mapping strategies should be implemented of this interface
 *
 * @author constantine.shtompel@gmail.com
 */
interface StrategyInterface
{
    /**
     * Map parameters to object
     *
     * @param PropertyMetadata $property
     * @param object           $object
     * @param mixed            $value
     */
    public function map(PropertyMetadata $property, $object, $value);
}