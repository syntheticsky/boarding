<?php

namespace Boarding\ObjectMapper\Annotation;


/**
 * Indicate property as collection for mapping
 *
 * @author constantine.shtompel@gmail.com
 */
class Collection
{
    /** @var string */
    public $class = 'ArrayObject';
    /** @var bool */
    public $saveKeys = true;
}