<?php

namespace Boarding\ObjectMapper;

use Boarding\ObjectMapper\Metadata\MetadataFactoryInterface;
use Boarding\ObjectMapper\Metadata\ObjectMetadata;


/**
 * All data mappers should be implemented of this interface
 *
 * @author constantine.shtompel@gmail.com
 */
interface ObjectMapperInterface
{
    /**
     * Is object supported
     *
     * @param object $object
     * @param string $group
     *
     * @return bool
     */
    public function isSupported($object, $group = ObjectMetadata::DEFAULT_GROUP);

    /**
     * Map parameters for object
     *
     * @param object $object
     * @param array  $parameters
     * @param string $group
     */
    public function map($object, array $parameters, $group = ObjectMetadata::DEFAULT_GROUP);
}