<?php

namespace Boarding\ObjectMapper\Exception;

use Exception;


/**
 * Exception for control mapping failed
 *
 * @author constantine.shtompel@gmail.com
 */
class MapException extends Exception
{
}
