<?php

namespace Boarding\ObjectMapper\Exception;

use Exception;


/**
 * Control object not supported for mapping
 *
 * @author constantine.shtompel@gmail.com
 */
class ObjectNotSupportedException extends Exception
{
}
