<?php

namespace Boarding\ObjectMapper\Exception;

use Exception;


/**
 * Control strategy not found error
 *
 * @author constantine.shtompel@gmail.com
 */
class StrategyNotFoundException extends Exception
{
}
